package com.here.heresdkplayground.location

import android.content.Context
import com.here.sdk.core.Location
import com.here.sdk.routing.Route
import io.reactivex.Observable

interface LocationInterface {

    val locationStream: Observable<Location>

    fun startUpdates(interval: Long = 500, route: Route? = null)
    fun stopUpdates()

}