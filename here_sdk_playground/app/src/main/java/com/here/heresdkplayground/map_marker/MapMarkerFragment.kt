package com.here.heresdkplayground.map_marker

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.here.heresdkplayground.R
import com.here.heresdkplayground.PermissionsRequestor
import com.here.sdk.mapview.MapScene
import com.here.sdk.mapview.MapScheme
import kotlinx.android.synthetic.main.fragment_map_marker.*

class MapMarkerFragment : Fragment() {

    companion object {
        private val TAG = MapMarkerFragment::class.java.simpleName
    }

    private lateinit var permissionsRequestor: PermissionsRequestor
    private var mapMarkerExample: MapMarkerExample? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_map_marker, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mapView.onCreate(savedInstanceState)

        handleAndroidPermissions()

        btnAnchor.setOnClickListener { mapMarkerExample?.showAnchoredMapMarkers() }
        btnCenter.setOnClickListener { mapMarkerExample?.showCenteredMapMarkers() }
        btnClear.setOnClickListener { mapMarkerExample?.clearMap() }
    }

    private fun handleAndroidPermissions() {
        permissionsRequestor = PermissionsRequestor(requireActivity())
        permissionsRequestor.request(object : PermissionsRequestor.ResultListener {
            override fun permissionsGranted() {
                loadMapScene()
            }

            override fun permissionsDenied() {
                Log.e(TAG, "Permissions DENIED!!!!")
            }
        })
    }

    override fun onResume() {
        super.onResume()

        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()

        mapView.onPause()
    }

    override fun onDestroyView() {
        super.onDestroyView()

        mapView.onDestroy()
    }

    fun loadMapScene() {
        mapView.mapScene.loadScene(MapScheme.NORMAL_DAY) {
            it?.let {
                Log.e(TAG, "Load Scene Failed: ${it.value}")
            } ?: run {
                mapMarkerExample = MapMarkerExample(requireContext(), mapView)
            }
        }
    }

}