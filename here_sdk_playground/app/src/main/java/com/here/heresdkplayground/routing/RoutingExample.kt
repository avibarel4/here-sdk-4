package com.here.heresdkplayground.routing

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.annotation.DrawableRes
import com.here.heresdkplayground.utils.MapUtils
import com.here.sdk.core.Color
import com.here.sdk.core.GeoCoordinates
import com.here.sdk.core.GeoPolyline
import com.here.sdk.core.errors.InstantiationErrorException
import com.here.sdk.mapview.MapImageFactory
import com.here.sdk.mapview.MapMarker
import com.here.sdk.mapview.MapPolyline
import com.here.sdk.mapview.MapView
import com.here.sdk.routing.*
import java.util.*


class RoutingExample(
    private val context: Context,
    private val mapView: MapView
) {

    companion object {
        private val TAG = RoutingExample::class.java.simpleName

        const val FACTOR = 10000.0
        const val LAT = 32.197816
        const val LON = 34.881095
    }

    private val mapMarkers = mutableListOf<MapMarker>()
    private val mapPolylines = mutableListOf<MapPolyline>()
    private val routingEngine: OlpRoutingEngine?
    private var originGeoCoordinates: GeoCoordinates? = null
    private var destinationGeoCoordinates: GeoCoordinates? = null

    init {
        mapView.camera.lookAt(GeoCoordinates(LAT, LON))

        routingEngine = try {
            OlpRoutingEngine()
        } catch (e: InstantiationErrorException) {
            Log.e(TAG, "Failed to init Routing Engine: ${e.error}")
            null
        }
    }

    fun addRoute() {
        clearMap()

        routingEngine?.let {
            originGeoCoordinates = createNewRelativeCoordinate(-0.01)
            destinationGeoCoordinates = createNewRelativeCoordinate(0.01)
            val originWaypoint = Waypoint(originGeoCoordinates!!)
            val destinationWaypoint = Waypoint(destinationGeoCoordinates!!)

            it.calculateRoute(
                listOf(originWaypoint, destinationWaypoint),
                OlpRoutingEngine.CarOptions()
            ) { routingError, routes ->
                routingError?.let {
                    Log.e(TAG, "Error Calculating Route")
                } ?: run {
                    routes?.firstOrNull()?.let { route ->
                        Log.d(TAG, "Found Route: $route")
                        showRouteDetails(route)

                        MapUtils.showRouteOnMap(mapView, mapPolylines, route, originGeoCoordinates!!, destinationGeoCoordinates!!, mapMarkers)
                        route.sections.forEach { section ->
                            logManeuverInstructions(section.maneuvers)
                        }
                    } ?: run {
                        Log.e(TAG, "No Routes Found!!!")
                    }
                }
            }
//            { routingError, routes ->
//                routingError?.let {
//                    Log.e(TAG, "Error Calculating Route")
//                } ?: run {
//                    routes?.firstOrNull()?.let { route ->
//                        Log.d(TAG, "Found Route: $route")
//                        showRouteDetails(route)
//                        showRouteOnMap(route)
//                    } ?: run {
//                        Log.e(TAG, "No Routes Found!!!")
//                    }
//                }
//            }
        }
    }

    fun addWaypoints() {
        if (originGeoCoordinates == null || destinationGeoCoordinates == null) {
            Log.e(TAG, "No Route Found.. Can't add waypoints")
            return
        }

        clearMap()

        val waypoint1 = Waypoint(createNewRelativeCoordinate(0.02))
        val waypoint2 = Waypoint(createNewRelativeCoordinate(0.04))

        routingEngine?.let {
            it.calculateRoute(
                listOf(Waypoint(originGeoCoordinates!!), waypoint1, waypoint2, Waypoint(destinationGeoCoordinates!!)),
                OlpRoutingEngine.CarOptions()
            ) { routingError, routes ->
                routingError?.let {
                    Log.e(TAG, "Error Calculating Route")
                } ?: run {
                    routes?.firstOrNull()?.let { route ->
                        Log.d(TAG, "Found Route: $route")
                        showRouteDetails(route)
                        MapUtils.showRouteOnMap(mapView, mapPolylines, route, originGeoCoordinates!!, destinationGeoCoordinates!!, mapMarkers)

                        MapUtils.addCircleMarker(mapView, waypoint1.coordinates, android.R.drawable.ic_dialog_info, mapMarkers)
                        MapUtils.addCircleMarker(mapView, waypoint2.coordinates, android.R.drawable.ic_dialog_info, mapMarkers)
                    } ?: run {
                        Log.e(TAG, "No Routes Found!!!")
                    }
                }
            }
        }
    }

    fun clearMap() {
        MapUtils.clearWaypointMapMarker(mapMarkers, mapView)
        MapUtils.clearRoute(mapPolylines, mapView)
    }

//    fun createRandomGeoCoordinatesInViewport(): GeoCoordinates {
////        val rect = Rect()
////        val geoBox = mapView.getHitRect(rect)
////
////        rect.t
//
//        return GeoCoordinates()
//    }

    private fun createNewRelativeCoordinate(delta: Double): GeoCoordinates {
        return GeoCoordinates(LAT + delta, LON + delta)
    }

//    private fun showRouteOnMap(route: Route) {
//        val geoPolyline = try {
//            GeoPolyline(route.polyline)
//        } catch (e: InstantiationErrorException) {
//            Log.e(TAG, "Failed to create Geo Polyline: ${e.error}")
//            return
//        }
//
//        val mapPolyline = MapPolyline(geoPolyline, /*Route Width*/10f, Color(0, 255, 0))
//        mapView.mapScene.addMapPolyline(mapPolyline)
//        mapPolylines.add(mapPolyline)
//
//        // origin
//        addCircleMarker(originGeoCoordinates!!, android.R.drawable.ic_dialog_map)
//        // destination
//        addCircleMarker(destinationGeoCoordinates!!, android.R.drawable.ic_menu_compass)
//
//        route.sections.forEach {
//            logManeuverInstructions(it.maneuvers)
//        }
//    }

//    private fun addCircleMarker(geoCoordinates: GeoCoordinates, @DrawableRes image: Int) {
//        val mapImage = MapImageFactory.fromResource(context.resources, image)
//        val mapMarker = MapMarker(geoCoordinates, mapImage)
//        mapView.mapScene.addMapMarker(mapMarker)
//        mapMarkers.add(mapMarker)
//    }

    private fun logManeuverInstructions(maneuvers: List<Maneuver>) {
        Log.d(TAG, "Log maneuver instructions per route leg:")
        for (maneuverInstruction in maneuvers) {
            val maneuverAction = maneuverInstruction.action
            val maneuverLocation = maneuverInstruction.coordinates
            val maneuverInfo = (maneuverInstruction.text
                    + ", Action: " + maneuverAction.name
                    + ", Location: " + maneuverLocation.toString())
            Log.d(TAG, maneuverInfo)
        }
    }

    private fun showRouteDetails(route: Route) {
        Toast.makeText(context, "Route Details:\nDuration: ${formatTime(route.durationInSeconds)} (${route.durationInSeconds})\nLength: ${formatLength(route.lengthInMeters)} (${route.lengthInMeters})", Toast.LENGTH_LONG).show()
    }

    private fun formatTime(sec: Long): String? {
        val hours = sec / 3600
        val minutes = sec % 3600 / 60
        return String.format(Locale.getDefault(), "%02d:%02d", hours, minutes)
    }

    private fun formatLength(meters: Int): String? {
        val kilometers = meters / 1000
        val remainingMeters = meters % 1000
        return java.lang.String.format(
            Locale.getDefault(),
            "%02d.%02d km",
            kilometers,
            remainingMeters
        )
    }
}