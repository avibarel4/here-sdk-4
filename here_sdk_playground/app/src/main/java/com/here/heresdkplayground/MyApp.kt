package com.here.heresdkplayground

import android.app.Application
import android.util.Log
import com.here.sdk.core.engine.SDKNativeEngine
import com.here.sdk.core.engine.SDKOptions
import com.here.sdk.engine.InitProvider
import java.lang.Exception

class MyApp : Application() {

    override fun onCreate() {
        super.onCreate()

        InitProvider.initialize(this)
        try {
            SDKNativeEngine.setSharedInstance(SDKNativeEngine(SDKOptions(BuildConfig.HERE_KEY_ID, BuildConfig.HERE_KEY_SECRET, "")))
            Log.d("MyApp", "Init Success") // TODO This Always succeeds.. the SDK should provide some error mechanism in the future.. hopefully...
        } catch (e: Exception) {
            Log.e("MyApp", "Failed to init: ${e.message}")
        }
    }

}