package com.here.heresdkplayground.routing

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.here.heresdkplayground.PermissionsRequestor
import com.here.heresdkplayground.R
import com.here.sdk.core.GeoCoordinates
import com.here.sdk.core.Point2D
import com.here.sdk.mapview.MapCamera
import com.here.sdk.mapview.MapScene
import com.here.sdk.mapview.MapScheme
import kotlinx.android.synthetic.main.fragment_routing.*

class RoutingFragment : Fragment() {

    companion object {
        private val TAG = RoutingFragment::class.java.simpleName
    }

    private lateinit var permissionsRequestor: PermissionsRequestor
    private var routingExample: RoutingExample? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_routing, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mapView.onCreate(savedInstanceState)

        handleAndroidPermissions()

        btnAddRoute.setOnClickListener { routingExample?.addRoute() }
        btnAddWaypoints.setOnClickListener { routingExample?.addWaypoints() }
        btnClearMap.setOnClickListener { routingExample?.clearMap() }

        btnZoomIn.setOnClickListener {
            addZoom(true)
        }
        btnZoomOut.setOnClickListener {
            addZoom(false)
        }
    }

    private fun addZoom(zoomIn: Boolean) {
        if (zoomIn) {
            mapView.camera.zoomBy(0.1, Point2D(RoutingExample.LAT, RoutingExample.LON))
        } else {
            mapView.camera.lookAt(GeoCoordinates(RoutingExample.LAT, RoutingExample.LON))
        }

//        val bearing = mapView.camera.state.targetOrientation.bearing
//        val tilt = mapView.camera.state.targetOrientation.tilt + if (zoomIn) 10 else -10
//        Log.d(TAG, "Current Tilt: $tilt")

//        mapView.camera.lookAt(GeoCoordinates(RoutingExample.LAT, RoutingExample.LON), MapCamera.OrientationUpdate(bearing, tilt), RoutingExample.FACTOR)



//        mapView.camera.lookAt(GeoCoordinates(RoutingExample.LAT, RoutingExample.LON))

//        Log.d(TAG, "Distance To Target 1 = ${mapView.camera.state.distanceToTargetInMeters}")
//
//        mapView.camera.state.targetCoordinates = GeoCoordinates(RoutingExample.LAT + 1, RoutingExample.LON + 1)
//
//        Log.d(TAG, "Distance To Target 2 = ${mapView.camera.state.distanceToTargetInMeters}")
//
//        Log.d(TAG, "New Tilt: ${mapView.camera.state.targetOrientation.tilt}")

//        with(mapView.camera.state.targetOrientation) {
//            mapView.camera.state.targetOrientation = MapCamera.Orientation(this.bearing, this.tilt + 1)//.zoomBy(currentZoom, Point2D(RoutingExample.LAT, RoutingExample.LON))
//        }

//        Log.d(TAG, "Current Zoom: $currentZoom")
    }

    override fun onResume() {
        super.onResume()

        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()

        mapView.onPause()
    }

    override fun onDestroyView() {
        super.onDestroyView()

        mapView.onDestroy()
    }

    private fun handleAndroidPermissions() {
        permissionsRequestor =
            PermissionsRequestor(requireActivity())

        permissionsRequestor.request(object : PermissionsRequestor.ResultListener {
            override fun permissionsGranted() {
                Log.d(TAG, "Permissions Granted.");
                loadMapScene()
            }

            override fun permissionsDenied() {
                Log.e(TAG, "Permissions denied by user.");
            }
        })
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        permissionsRequestor.onRequestPermissionsResult(requestCode, grantResults)
    }

    fun loadMapScene() {
        mapView.mapScene.loadScene(MapScheme.NORMAL_DAY, MapScene.LoadSceneCallback { mapError ->
            mapError?.let {
                Log.e(TAG, "onLoadScene Failed: ${it.value}")
            } ?: run {
                routingExample = RoutingExample(requireContext(), mapView)
            }
        })
    }

}