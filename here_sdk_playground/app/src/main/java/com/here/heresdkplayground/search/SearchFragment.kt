package com.here.heresdkplayground.search

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.here.heresdkplayground.PermissionsRequestor
import com.here.heresdkplayground.R
import com.here.sdk.mapview.MapScheme
import kotlinx.android.synthetic.main.fragment_search.*

class SearchFragment : Fragment() {

    companion object {
        private val TAG = SearchFragment::class.java.simpleName
    }

    private lateinit var permissionsRequestor: PermissionsRequestor
    private var searchExample: SearchExample? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mapView.onCreate(savedInstanceState)

        btnSearch.setOnClickListener { searchExample?.searchClicked(editSearch.text.toString()) }
        btnGeoCoding.setOnClickListener { searchExample?.geocodeClicked() }

        handleAndroidPermissions()
    }

    private fun handleAndroidPermissions() {
        permissionsRequestor = PermissionsRequestor(requireActivity())
        permissionsRequestor.request(object : PermissionsRequestor.ResultListener {
            override fun permissionsGranted() {
                loadMapScene()
            }

            override fun permissionsDenied() {
                Log.e(TAG, "Permissions DENIED!!!!")
            }
        })
    }

    private fun loadMapScene() {
        mapView.mapScene.loadScene(MapScheme.NORMAL_DAY) {
            it?.let {
                Log.e(TAG, "Load Scene Failed: ${it.value}")
            } ?: run {
                searchExample = SearchExample(requireContext(), mapView)
            }
        }
    }

    override fun onResume() {
        super.onResume()

        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()

        mapView.onPause()
    }

    override fun onDestroyView() {
        super.onDestroyView()

        mapView.onDestroy()
    }
}