package com.here.heresdkplayground.utils

import android.util.Log
import androidx.annotation.DrawableRes
import com.here.sdk.core.Color
import com.here.sdk.core.GeoCoordinates
import com.here.sdk.core.GeoPolyline
import com.here.sdk.core.errors.InstantiationErrorException
import com.here.sdk.mapview.MapImageFactory
import com.here.sdk.mapview.MapMarker
import com.here.sdk.mapview.MapPolyline
import com.here.sdk.mapview.MapView
import com.here.sdk.routing.Route

object MapUtils {

    private const val TAG = "MapUtils"

    fun showRouteOnMap(mapView: MapView, mapPolylines: MutableList<MapPolyline>, route: Route, originGeoCoordinates: GeoCoordinates, destinationGeoCoordinates: GeoCoordinates, mapMarkers: MutableList<MapMarker>) {
        val geoPolyline = try {
            GeoPolyline(route.polyline)
        } catch (e: InstantiationErrorException) {
            Log.e(TAG, "Failed to create Geo Polyline: ${e.error}")
            return
        }

        val mapPolyline = MapPolyline(geoPolyline, /*Route Width*/10f, Color(0, 255, 0))
        mapView.mapScene.addMapPolyline(mapPolyline)
        mapPolylines.add(mapPolyline)

        // origin
        addCircleMarker(mapView, originGeoCoordinates, android.R.drawable.ic_dialog_map, mapMarkers)
        // destination
        addCircleMarker(mapView, destinationGeoCoordinates, android.R.drawable.ic_menu_compass, mapMarkers)
    }

    fun addSingleMarker(mapView: MapView, geoCoordinates: GeoCoordinates, @DrawableRes image: Int): MapMarker {
        val mapImage = MapImageFactory.fromResource(mapView.resources, image)
        val mapMarker = MapMarker(geoCoordinates, mapImage)
        mapView.mapScene.addMapMarker(mapMarker)
        return mapMarker
    }

    fun addCircleMarker(mapView: MapView, geoCoordinates: GeoCoordinates, @DrawableRes image: Int, mapMarkers: MutableList<MapMarker>) {
        val mapImage = MapImageFactory.fromResource(mapView.resources, image)
        val mapMarker = MapMarker(geoCoordinates, mapImage)
        mapView.mapScene.addMapMarker(mapMarker)
        mapMarkers.add(mapMarker)
    }

    fun clearWaypointMapMarker(mapMarkers: MutableList<MapMarker>, mapView: MapView) {
        mapMarkers.forEach {
            mapView.mapScene.removeMapMarker(it)
        }
        mapMarkers.clear()
    }

    fun clearRoute(mapPolylines: MutableList<MapPolyline>, mapView: MapView) {
        mapPolylines.forEach {
            mapView.mapScene.removeMapPolyline(it)
        }
        mapPolylines.clear()
    }

}