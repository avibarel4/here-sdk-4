package com.here.heresdkplayground.location

import android.content.Context
import com.google.android.gms.location.*
import com.here.sdk.core.GeoCoordinates
import com.here.sdk.core.Location
import com.here.sdk.routing.Route
import io.reactivex.subjects.BehaviorSubject
import java.util.*

class LocationProvider(context: Context) : LocationCallback(), LocationInterface {

    override val locationStream = BehaviorSubject.create<Location>()

    private lateinit var fusedLocation: FusedLocationProviderClient

    init {
        fusedLocation = LocationServices.getFusedLocationProviderClient(context)
    }

    override fun startUpdates(interval: Long, route: Route?) {
        val locationRequest = LocationRequest().apply {
            this.interval = interval
        }

        fusedLocation.requestLocationUpdates(locationRequest, this, null)
    }

    override fun stopUpdates() {
        fusedLocation.removeLocationUpdates(this)
    }

    override fun onLocationAvailability(locationAvailability: LocationAvailability) {
        super.onLocationAvailability(locationAvailability)
    }

    override fun onLocationResult(locationResult: LocationResult) {
        super.onLocationResult(locationResult)

        with(locationResult.lastLocation) {
            locationStream.onNext(Location(GeoCoordinates(latitude, longitude), bearing.toDouble(), null, Date(time), null, null))
        }
    }

}