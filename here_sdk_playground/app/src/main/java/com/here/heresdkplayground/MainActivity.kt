package com.here.heresdkplayground

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.here.heresdkplayground.map_marker.MapMarkerFragment
import com.here.heresdkplayground.navigation.NavigationFragment
import com.here.heresdkplayground.routing.RoutingFragment
import com.here.heresdkplayground.search.SearchFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnRouting.setOnClickListener { loadScreen(RoutingFragment()) }
        btnMapMarker.setOnClickListener { loadScreen(MapMarkerFragment()) }
        btnSearch.setOnClickListener { loadScreen(SearchFragment()) }
        btnNavigate.setOnClickListener { loadScreen(NavigationFragment()) }
    }

    private fun loadScreen(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, fragment)
            .addToBackStack(null)
            .commit()
    }

}
