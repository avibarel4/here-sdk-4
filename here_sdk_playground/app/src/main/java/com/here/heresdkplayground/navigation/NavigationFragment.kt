package com.here.heresdkplayground.navigation

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.here.heresdkplayground.R
import com.here.heresdkplayground.location.LocationInterface
import com.here.heresdkplayground.location.LocationMock
import com.here.heresdkplayground.location.LocationProvider
import com.here.heresdkplayground.utils.MapUtils
import com.here.sdk.core.GeoCoordinates
import com.here.sdk.mapview.MapCamera
import com.here.sdk.mapview.MapMarker
import com.here.sdk.mapview.MapPolyline
import com.here.sdk.mapview.MapScheme
import com.here.sdk.navigation.*
import com.here.sdk.routing.OlpRoutingEngine
import com.here.sdk.routing.Route
import com.here.sdk.routing.Waypoint
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_navigation.*
import java.lang.Exception

class NavigationFragment : Fragment(), TrackingNavigatorListener {

    companion object {
        private val TAG = NavigationFragment::class.java.simpleName

        private const val FACTOR = 10000.0
        private const val LAT = 32.197816
        private const val LON = 34.881095

    }

    private val mapMarkers = mutableListOf<MapMarker>()
    private val mapPolylines = mutableListOf<MapPolyline>()

    private var routingNavigator: RouteNavigator? = null

//    private var trackingController: TrackingController? = null
//    private var trackingNavigator: TrackingNavigator? = null

    private lateinit var locationProvider: LocationInterface

    private var currentMarker: MapMarker? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_navigation, container, false)
    }

    private enum class RouteType {
        NAVIGATION, MOCK_NAVIGATION
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        btnStartNavigation.isEnabled = false
        btnMockNavigation.isEnabled = false

        btnStartNavigation.setOnClickListener { calcRoute(RouteType.NAVIGATION) }
        btnMockNavigation.setOnClickListener { calcRoute(RouteType.MOCK_NAVIGATION) }

//        btnStartTracking.setOnClickListener { calcRoute(RouteType.TRACKING) }

        mapView.onCreate(savedInstanceState)


        mapView.mapScene.loadScene(MapScheme.NORMAL_DAY) {
            it?.let {
                Log.e(TAG, "Failed to Load Scene: ${it.value}")
            } ?: run {
                mapView.camera.lookAt(GeoCoordinates(LAT, LON), FACTOR)
                btnStartNavigation.isEnabled = true
                btnMockNavigation.isEnabled = true
            }
        }
    }

    private fun calcRoute(routeType: RouteType, extraPoint: Waypoint? = null) {

        clearMap()

//        if (trackingController?.isRunning == true) {
//            trackingNavigator?.removeTrackingListener(this)
//            trackingController?.stop()
//            trackingController = null
//        }

        val wp1 = Waypoint(GeoCoordinates(32.191227, 34.892222)) // Green Mall
        val wp2 = Waypoint(GeoCoordinates(32.197782, 34.882458)) // Zarhin

        val wayPoints = extraPoint?.let { listOf(it, wp1, wp2) } ?: listOf(wp1, wp2)

        val olpRoutingEngine = OlpRoutingEngine()
        olpRoutingEngine.calculateRoute(
            wayPoints,
            OlpRoutingEngine.CarOptions()
        ) { routingError, routes ->

            routingError?.let {
                Log.e(TAG, "Failed to calc route")
            } ?: run {
                routes?.firstOrNull()?.let {
                    when (routeType) {
                        RouteType.NAVIGATION,
                        RouteType.MOCK_NAVIGATION -> {
                            startNavigation(it, routeType)

                            btnStartNavigation.visibility = View.GONE
                            btnMockNavigation.visibility = View.GONE
                        }
                    }

                    MapUtils.showRouteOnMap(mapView, mapPolylines, it, wp1.coordinates, wp2.coordinates, mapMarkers)
                }
            } ?: Log.d(TAG, "No Routes Found :[")
        }
    }

    private fun clearMap() {
        currentMarker?.let { mapMarkers.add(it) } // don't forget this one
        MapUtils.clearWaypointMapMarker(mapMarkers, mapView)
        MapUtils.clearRoute(mapPolylines, mapView)
    }

    private fun startNavigation(route: Route, routeType: RouteType) {

        clearMap()

        locationProvider = when (routeType) {
            RouteType.NAVIGATION -> {
                LocationProvider(requireContext())
            }
            RouteType.MOCK_NAVIGATION -> {
                LocationMock()
            }
        }

        registerForUpdates()

        routingNavigator = try {
            RouteNavigator(route)
        } catch (e: Exception) {
            Log.e(TAG, "Failed to init RouteNavigator: ${e.message}")
            null
        }

        Log.d(TAG, "Started Navigation??? - Result: $routingNavigator")

        addNavigationListeners(route)
    }

    private fun registerForUpdates() {
        val disposable = locationProvider.locationStream.subscribe({ location ->
            Log.d(TAG, "Location Stream Updated: $location")

            routingNavigator?.onLocationUpdated(location)
            val coords = GeoCoordinates(location.coordinates.latitude, location.coordinates.longitude)
            mapView.camera.lookAt(coords, MapCamera.OrientationUpdate(location.bearingInDegrees, 100.0), FACTOR / 50)

            // add current marker
            val isFirstUpdate = currentMarker == null
            currentMarker?.let { mapView.mapScene.removeMapMarker(it) }
            currentMarker = MapUtils.addSingleMarker(mapView, coords, R.drawable.circle)

            if (isFirstUpdate) {
                calcRoute(RouteType.NAVIGATION, Waypoint(coords))
            }
        },{
            Log.e(TAG, "Error in Location Stream: ${it.message}")
        })

        compositeDisposable.add(disposable)
    }

    private val compositeDisposable = CompositeDisposable()

    override fun onDestroyView() {
        super.onDestroyView()

        mapView.onDestroy()

        removeNavigationListeners()
    }

    override fun onTrackingUpdated(trackingNavigatorEvent: TrackingNavigatorEvent) {
        Log.d(TAG, "Track Update: $trackingNavigatorEvent")
        Log.d(TAG, "Speed Limit: ${trackingNavigatorEvent.speedLimitInKilometersPerHour}")
        Log.d(TAG, "Street Name: ${trackingNavigatorEvent.streetName}")
        Log.d(TAG, "Street Name: ${trackingNavigatorEvent.streetName}")
    }

    private fun addNavigationListeners(route: Route?) {

        locationProvider.startUpdates(500, route)

        routingNavigator?.addRouteProgressListener(routeProgressListener)
        routingNavigator?.addNextManeuverListener(nextManeuverListener)
        routingNavigator?.addRouteDeviationListener(routeDeviationListener)
    }

    private fun removeNavigationListeners() {

        locationProvider.stopUpdates()

        routingNavigator?.removeRouteProgressListener(routeProgressListener)
        routingNavigator?.removeNextManeuverListener(nextManeuverListener)
        routingNavigator?.removeRouteDeviationListener(routeDeviationListener)
    }

    private val routeProgressListener = RouteProgressListener { progress ->
        Log.d(
            TAG,
            "Progress - Index: ${progress.sectionIndex}, Remaining Distance: ${progress.remainingDistanceInMeters}"
        )
    }

    private val nextManeuverListener = NextManeuverListener { param1, param2 ->
        Log.d(TAG, "Next Maneuver - Param 1: $param1, Param 2: $param2")
    }

    private val routeDeviationListener = RouteDeviationListener { deviation ->
        Log.d(
            TAG,
            "Deviation: CurrentLocation: ${deviation.currentLocation}, LastLocationOnRoute: ${deviation.lastLocationOnRoute}"
        )
    }
}