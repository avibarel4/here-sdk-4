package com.here.heresdkplayground.search

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.here.heresdkplayground.R
import com.here.sdk.core.Anchor2D
import com.here.sdk.core.GeoCoordinates
import com.here.sdk.core.LanguageCode
import com.here.sdk.core.Point2D
import com.here.sdk.core.errors.InstantiationErrorException
import com.here.sdk.gestures.GestureState
import com.here.sdk.mapview.MapImageFactory
import com.here.sdk.mapview.MapMarker
import com.here.sdk.mapview.MapView
import com.here.sdk.search.OpenAutosuggestEngine
import com.here.sdk.search.OpenGeocodingEngine
import com.here.sdk.search.OpenReverseGeocodingEngine
import com.here.sdk.search.OpenSearchEngine


class SearchExample(
    private val context: Context,
    private val mapView: MapView
) {

    companion object {
        private val TAG = SearchExample::class.java.simpleName

        private const val FACTOR = 10000.0
        private const val LAT = 32.197816
        private const val LON = 34.881095
    }

    private val searchEngine: OpenSearchEngine?
    private val autuSuggestionEngine: OpenAutosuggestEngine?
    private val geocodingEngine: OpenGeocodingEngine?
    private val reverseGeocodingEngine: OpenReverseGeocodingEngine?

    private var lastSuggestionResult: OpenAutosuggestEngine.Result? = null

    private val mapMarkers = mutableListOf<MapMarker>()

    init {
        mapView.camera.lookAt(GeoCoordinates(LAT, LON), FACTOR)

        searchEngine = try {
            OpenSearchEngine()
        } catch (e: InstantiationErrorException) {
            Log.e(TAG, "Failed to init Search Engine: ${e.error}")
            null
        }

        autuSuggestionEngine = try {
            OpenAutosuggestEngine()
        } catch (e: InstantiationErrorException) {
            Log.e(TAG, "Failed to init Auto Suggest Engine: ${e.error}")
            null
        }

        geocodingEngine = try {
            OpenGeocodingEngine()
        } catch (e: InstantiationErrorException) {
            Log.e(TAG, "Failed to init Geocode Engine: ${e.error}")
            null
        }

        reverseGeocodingEngine = try {
            OpenReverseGeocodingEngine()
        } catch (e: InstantiationErrorException) {
            Log.e(TAG, "Failed to init Reverse Geocode Engine: ${e.error}")
            null
        }

        mapView.gestures.setTapListener {
            pickMapMarker(it)
        }

        mapView.gestures.setLongPressListener { gestureState, touchPoint ->
            if (gestureState == GestureState.BEGIN) {
                val geoCoordinates = mapView.viewToGeoCoordinates(touchPoint)
                geoCoordinates?.let {
                    addPoiMapMarker(geoCoordinates)
                    getAddressForCoordinates(geoCoordinates)
                }
            }
        }
    }

    fun searchClicked(searchTerm: String) {
        searchInViewPort(searchTerm)
        autoSuggest(searchTerm)
    }

    fun geocodeClicked() {
        lastSuggestionResult?.let { result ->
            result.coordinates?.let {
                Toast.makeText(context, "Focusing on ${result.title}", Toast.LENGTH_LONG).show()

                mapView.camera.lookAt(it, FACTOR)
                geocodeAddressInViewPort(result.title)
            }
        }
    }

    private fun createNewRelativeCoordinate(delta: Double): GeoCoordinates {
        return GeoCoordinates(LAT + delta, LON + delta)
    }

    private fun pickMapMarker(touchPoint: Point2D) {
        mapView.pickMapItems(touchPoint, 2f) {
            it?.markers?.firstOrNull()?.metadata?.let {  topMarkerMetadata ->
                (topMarkerMetadata.getCustomValue("key_search_result"))?.let {
                    Toast.makeText(context, "Picked Search Result: Title: $it", Toast.LENGTH_LONG).show()
                }
            } ?: run {
                val marker = it?.markers?.firstOrNull()
                val message = "Map Marker Picked 2: \nLocation: ${marker?.coordinates?.latitude}, ${marker?.coordinates?.longitude}"
                Toast.makeText(context, message, Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun addPoiMapMarker(geoCoordinates: GeoCoordinates) {
        val mapMarker = createPoiMapMarker(geoCoordinates)
        mapView.mapScene.addMapMarker(mapMarker)
        mapMarkers.add(mapMarker)
    }

    private fun createPoiMapMarker(geoCoordinates: GeoCoordinates): MapMarker {
        val mapImage = MapImageFactory.fromResource(context.resources, R.drawable.poi)
        val mapMarker = MapMarker(geoCoordinates, mapImage, Anchor2D(0.5f, 1f))
        return mapMarker
    }

    private fun getAddressForCoordinates(geoCoordinates: GeoCoordinates) {
        reverseGeocodingEngine?.searchAddress(geoCoordinates, OpenReverseGeocodingEngine.Options(LanguageCode.HE_IL)) { searchError, address ->
            searchError?.let {
                Log.e(TAG, "Failed to Reverse GeoCode: ${it.value}")
            } ?: run {
                Toast.makeText(context, "Reverse Geo Address: $address", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun autoSuggest(searchTerm: String) {
        autuSuggestionEngine?.suggest(
            searchTerm,
            GeoCoordinates(LAT, LON),
            OpenAutosuggestEngine.Options(LanguageCode.HE_IL, 5)
        ) { searchError, autoSuggestResults ->
            searchError?.let {
                Log.e(TAG, "Failed to get Auto Suggest Results: ${it.value}")
            } ?: run {
                val message = "List Size: ${autoSuggestResults?.size ?: "No Items!!!"}\n${autoSuggestResults?.map { 
                    "\n\n$Result: ${it.title}\nDistance: ${it.distanceInMeters}"
                }}"
                Toast.makeText(context, message, Toast.LENGTH_LONG).show()

                lastSuggestionResult = autoSuggestResults?.firstOrNull()
            }
        }
    }

    private fun geocodeAddressInViewPort(streetName: String) {
        clearMap()
    }

    private fun searchInViewPort(searchTerm: String) {
        clearMap()


    }

    private fun clearMap() {
        mapMarkers.forEach {
            mapView.mapScene.removeMapMarker(it)
        }

        mapMarkers.clear()
    }

}