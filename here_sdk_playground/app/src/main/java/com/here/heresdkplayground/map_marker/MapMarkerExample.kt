package com.here.heresdkplayground.map_marker

import android.content.Context
import android.widget.Toast
import com.here.heresdkplayground.R
import com.here.sdk.core.Anchor2D
import com.here.sdk.core.GeoCoordinates
import com.here.sdk.core.Metadata
import com.here.sdk.core.Point2D
import com.here.sdk.mapview.MapImageFactory
import com.here.sdk.mapview.MapMarker
import com.here.sdk.mapview.MapView

class MapMarkerExample(
    private val context: Context,
    private val mapView: MapView
) {

    companion object {
        private val TAG = MapMarkerExample::class.java.simpleName

        private const val FACTOR = 10000.0
        private const val LAT = 32.197816
        private const val LON = 34.881095
    }

    private val mapMarkers = mutableListOf<MapMarker>()

    init {
        mapView.camera.lookAt(GeoCoordinates(LAT, LON), FACTOR)

        setTapGestureHandler()
    }

    private fun setTapGestureHandler() {
        mapView.gestures.setTapListener {
            pickMapMarker(it)
        }
    }

    private fun pickMapMarker(touchPoint: Point2D) {
        mapView.pickMapItems(touchPoint, 2f) {
            it?.markers?.firstOrNull()?.metadata?.let {  topMarkerMetadata ->
                topMarkerMetadata.getString("key_poi")?.let {
                    Toast.makeText(context, "Map Marker Picked: $it", Toast.LENGTH_LONG).show()
                }
            } ?: run {
                val marker = it?.markers?.firstOrNull()
                val message = "Map Marker Picked 2: \nLocation: ${marker?.coordinates?.latitude}, ${marker?.coordinates?.longitude}"
                Toast.makeText(context, message, Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun createNewRelativeCoordinate(delta: Double): GeoCoordinates {
        return GeoCoordinates(LAT + delta, LON + delta)
    }


    fun showAnchoredMapMarkers() {

        clearMap()

        for (i in 1..3) {
            val geoCoordinates = createNewRelativeCoordinate(i.toDouble() / 100.0)

            // centered on location
            addCircleMapMarker(geoCoordinates)

            // anchored - pointing to location
            addPOIMapMarker(geoCoordinates)
        }
    }

    fun showCenteredMapMarkers() {

        clearMap()

        val geoCoordinates = createNewRelativeCoordinate(0.0)

        // Centered on location.
        addPhotoMapMarker(geoCoordinates);

        // Centered on location. Shown on top of the previous image to indicate the location.
        addCircleMapMarker(geoCoordinates);

    }

    fun clearMap() {
        mapMarkers.forEach {
            mapView.mapScene.removeMapMarker(it)
        }
        mapMarkers.clear()
    }


    private fun addCircleMapMarker(geoCoordinates: GeoCoordinates) {
        val mapImage = MapImageFactory.fromResource(context.resources, R.drawable.circle)
        val mapMarker = MapMarker(geoCoordinates, mapImage)
        mapView.mapScene.addMapMarker(mapMarker)
        mapMarkers.add(mapMarker)
    }

    private fun addPOIMapMarker(geoCoordinates: GeoCoordinates) {
        val mapImage = MapImageFactory.fromResource(context.resources, R.drawable.poi)
        val mapMarker = MapMarker(geoCoordinates, mapImage, Anchor2D(0.5f, 1f))
        mapMarker.metadata = Metadata().apply {
            setString("key_poi", "My Custom POI :]")
        }

        mapView.mapScene.addMapMarker(mapMarker)
        mapMarkers.add(mapMarker)
    }

    private fun addPhotoMapMarker(geoCoordinates: GeoCoordinates) {
        val mapImage = MapImageFactory.fromResource(context.resources, R.drawable.here_car)
        val mapMarker = MapMarker(geoCoordinates, mapImage)
        mapView.mapScene.addMapMarker(mapMarker)
        mapMarkers.add(mapMarker)
    }

}