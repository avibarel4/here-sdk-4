package com.here.heresdkplayground.location

import com.here.sdk.core.GeoCoordinates
import com.here.sdk.core.Location
import com.here.sdk.navigation.LocationListener
import com.here.sdk.navigation.LocationSimulator
import com.here.sdk.navigation.LocationSimulatorOptions
import com.here.sdk.routing.Route
import io.reactivex.subjects.BehaviorSubject

class LocationMock : LocationInterface, LocationListener {

    private var locationSimulator: LocationSimulator? = null

    override val locationStream = BehaviorSubject.create<Location>()

    override fun startUpdates(interval: Long, route: Route?) {
        route?.let {
            locationSimulator = LocationSimulator(it, LocationSimulatorOptions(1.0, interval))
        }

        locationSimulator?.let {
            it.listener = this
        }

        locationSimulator?.start()
    }

    override fun stopUpdates() {
        locationSimulator?.stop()
        locationSimulator?.listener = null
    }

    override fun onLocationTimeout() {

    }

    override fun onLocationUpdated(location: Location) {
        with(location) {
            locationStream.onNext(Location(GeoCoordinates(coordinates.latitude, coordinates.longitude), bearingInDegrees, null, timestamp, null, null))
        }
    }

}